"use strict";
require("typings-test");
let shelljs = require('shelljs');
const path = require("path");
const should = require("should");
const smartgit = require("../dist/index");
let paths = {
    temp: path.resolve('./test/temp/'),
    temp2: path.resolve('./test/temp2/'),
    temp3: path.resolve('./test/temp3'),
    temp4: path.resolve('./test/temp4'),
    noGit: path.resolve('./test/')
};
describe('smartgit', function () {
    let testGitRepo;
    let testGitRepoCloned;
    let testGitRepoInit;
    describe('instance', function () {
        it('should create a valid new instance from path', function () {
            testGitRepo = new smartgit.GitRepo(paths.temp);
            should(testGitRepo).be.instanceOf(smartgit.GitRepo);
        });
        it('should clone a repository using ssh and sshkey', function (done) {
            this.timeout(40000);
            smartgit.createRepoFromClone('git@gitlab.com:sandboxzone/sandbox-testrepo.git', paths.temp)
                .then((gitRepo) => {
                should(gitRepo).be.instanceOf(smartgit.GitRepo);
                done();
            }).catch(err => {
                throw err;
            });
        });
        it('should clone a repository using https', function (done) {
            this.timeout(40000);
            smartgit.createRepoFromClone('https://gitlab.com/sandboxzone/sandbox-testrepo.git', paths.temp2)
                .then((gitRepo) => {
                should(gitRepo).be.instanceOf(smartgit.GitRepo);
                done();
            }).catch(err => {
                throw err;
            });
        });
    });
    describe('.add', function () {
        it('should add a file to an existing repository', function () {
            shelljs.exec(`(cd ${paths.temp} && cp ../test.js .)`);
            testGitRepo.addAll(paths.temp);
        });
    });
    describe('.check()', function (done) {
        it('should check a git repo', function () {
            let checkResult = testGitRepo.check();
            should(checkResult).be.true();
        });
    });
    describe('commit', function () {
        it('should commit a new file to an existing repository', function () {
            testGitRepo.commit('added a new file');
        });
    });
    describe('pull', function () {
        this.timeout(40000);
        it('should pull from origin', function (done) {
            testGitRepo.pull()
                .then(() => {
                done();
            });
        });
    });
    describe('push', function () {
        this.timeout(40000);
        it('should push to origin', function (done) {
            testGitRepo.push('origin', 'master')
                .then(() => {
                done();
            });
        });
    });
    describe('remote', function () {
        it('should add a remote', function () {
            testGitRepo.remoteAdd('origin2', 'https://github.com/pushrocks/somerepo');
        });
        it('should', function (done) {
            testGitRepo.remoteList()
                .then(() => {
                done();
            });
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHdCQUFxQjtBQUVyQixJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDaEMsNkJBQTZCO0FBQzdCLGlDQUFnQztBQUVoQywwQ0FBMEM7QUFDMUMsSUFBSSxLQUFLLEdBQUc7SUFDUixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUM7SUFDbEMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDO0lBQ3BDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQztJQUNuQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUM7SUFDbkMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO0NBQ2pDLENBQUE7QUFJRCxRQUFRLENBQUMsVUFBVSxFQUFFO0lBQ2pCLElBQUksV0FBNkIsQ0FBQTtJQUNqQyxJQUFJLGlCQUFtQyxDQUFBO0lBQ3ZDLElBQUksZUFBaUMsQ0FBQTtJQUNyQyxRQUFRLENBQUMsVUFBVSxFQUFFO1FBQ2pCLEVBQUUsQ0FBQyw4Q0FBOEMsRUFBRTtZQUMvQyxXQUFXLEdBQUcsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUM5QyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDdkQsQ0FBQyxDQUFDLENBQUE7UUFDRixFQUFFLENBQUMsZ0RBQWdELEVBQUUsVUFBVSxJQUFJO1lBQy9ELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDbkIsUUFBUSxDQUFDLG1CQUFtQixDQUFDLGlEQUFpRCxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUM7aUJBQ3RGLElBQUksQ0FBQyxDQUFDLE9BQU87Z0JBQ1YsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFBO2dCQUMvQyxJQUFJLEVBQUUsQ0FBQTtZQUNWLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHO2dCQUNSLE1BQU0sR0FBRyxDQUFBO1lBQ2IsQ0FBQyxDQUFDLENBQUE7UUFDVixDQUFDLENBQUMsQ0FBQTtRQUNGLEVBQUUsQ0FBQyx1Q0FBdUMsRUFBRSxVQUFVLElBQUk7WUFDdEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUNuQixRQUFRLENBQUMsbUJBQW1CLENBQUMscURBQXFELEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQztpQkFDM0YsSUFBSSxDQUFDLENBQUMsT0FBTztnQkFDVixNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7Z0JBQy9DLElBQUksRUFBRSxDQUFBO1lBQ1YsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUc7Z0JBQ1IsTUFBTSxHQUFHLENBQUE7WUFDYixDQUFDLENBQUMsQ0FBQTtRQUNWLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQyxDQUFDLENBQUE7SUFDRixRQUFRLENBQUMsTUFBTSxFQUFFO1FBQ2IsRUFBRSxDQUFDLDZDQUE2QyxFQUFFO1lBQzlDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxLQUFLLENBQUMsSUFBSSxzQkFBc0IsQ0FBQyxDQUFBO1lBQ3JELFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ2xDLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQyxDQUFDLENBQUE7SUFDRixRQUFRLENBQUMsVUFBVSxFQUFFLFVBQVMsSUFBSTtRQUM5QixFQUFFLENBQUMseUJBQXlCLEVBQUU7WUFDMUIsSUFBSSxXQUFXLEdBQUcsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFBO1lBQ3JDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDakMsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDLENBQUMsQ0FBQTtJQUNGLFFBQVEsQ0FBQyxRQUFRLEVBQUU7UUFDZixFQUFFLENBQUMsb0RBQW9ELEVBQUU7WUFDckQsV0FBVyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1FBQzFDLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQyxDQUFDLENBQUE7SUFDRixRQUFRLENBQUMsTUFBTSxFQUFFO1FBQ2IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNuQixFQUFFLENBQUMseUJBQXlCLEVBQUUsVUFBVSxJQUFJO1lBQ3hDLFdBQVcsQ0FBQyxJQUFJLEVBQUU7aUJBQ2IsSUFBSSxDQUFDO2dCQUNGLElBQUksRUFBRSxDQUFBO1lBQ1YsQ0FBQyxDQUFDLENBQUE7UUFDVixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUMsQ0FBQyxDQUFBO0lBQ0YsUUFBUSxDQUFDLE1BQU0sRUFBRTtRQUNiLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDbkIsRUFBRSxDQUFDLHVCQUF1QixFQUFFLFVBQVUsSUFBSTtZQUN0QyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUM7aUJBQy9CLElBQUksQ0FBQztnQkFDRixJQUFJLEVBQUUsQ0FBQTtZQUNWLENBQUMsQ0FBQyxDQUFBO1FBQ1YsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDLENBQUMsQ0FBQTtJQUNGLFFBQVEsQ0FBQyxRQUFRLEVBQUU7UUFDZixFQUFFLENBQUMscUJBQXFCLEVBQUU7WUFDdEIsV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsdUNBQXVDLENBQUMsQ0FBQTtRQUM3RSxDQUFDLENBQUMsQ0FBQTtRQUNGLEVBQUUsQ0FBQyxRQUFRLEVBQUUsVUFBUyxJQUFJO1lBQ3RCLFdBQVcsQ0FBQyxVQUFVLEVBQUU7aUJBQ25CLElBQUksQ0FBQztnQkFDRixJQUFJLEVBQUUsQ0FBQTtZQUNWLENBQUMsQ0FBQyxDQUFBO1FBQ1YsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUMsQ0FBQyxDQUFBIn0=